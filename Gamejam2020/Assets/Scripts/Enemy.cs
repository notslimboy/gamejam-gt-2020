using System;
using UnityEngine;


public class Enemy : MonoBehaviour
{
    [SerializeField] private Transform playerPos;
    [SerializeField] private float moveSpeed;
    private Rigidbody2D _rigidbody2D;
    private Vector2 movement;

    private void Start()
    {
        playerPos = GameObject.Find("Char").gameObject.transform;
        _rigidbody2D = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        Vector3 direction = playerPos.position - this.transform.position; // pos enemy to char pos
        float angleLook = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        _rigidbody2D.rotation = angleLook;
        direction.Normalize();
        movement = direction;
    }

    private void FixedUpdate()
    {
        Move(movement);
    }

    void Move(Vector2 direction)
    {
        _rigidbody2D.MovePosition((Vector2)transform.position + (direction * moveSpeed * Time.deltaTime));
    }
}
