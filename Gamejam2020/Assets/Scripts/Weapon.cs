﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    [SerializeField] Transform firePos;
    [SerializeField] private GameObject bullet;


    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            ShootingProjectile();
        }
    }

    private void ShootingProjectile()
    {
        Instantiate(bullet, firePos.position, firePos.rotation); // spawn object
    }
}
