using System;
using UnityEngine;
using Random = UnityEngine.Random;


public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private float spawnTime = 3f;
    [SerializeField] private float spawnDelay = 5f;
    [SerializeField] private GameObject[] EnemyToSpawn;
    [SerializeField] private float minXPox;
    [SerializeField] private float maxXPos;
    [SerializeField] private float minYPox;
    [SerializeField] private float maxYPos;

    private void Start()
    {
        InvokeRepeating("SpawnEnemy",spawnDelay,spawnTime);
    }
    
    private void SpawnEnemy()
    {
        var enemyIndex = Random.Range(0, EnemyToSpawn.Length);
        var randSpawnPosX = Random.Range(minXPox, maxXPos);
        var randSpawnPosY = Random.Range(minYPox, maxYPos);
        Vector3 spawnPos = new Vector3(randSpawnPosX,randSpawnPosY);
        Instantiate(EnemyToSpawn[enemyIndex], spawnPos, transform.rotation);
    }
}
