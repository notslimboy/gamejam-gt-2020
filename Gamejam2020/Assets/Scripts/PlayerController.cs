﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private Rigidbody2D _rigidbody2D;
    [SerializeField] private Vector2 mousePos;
    [SerializeField] private Vector2 movement;
    [SerializeField] private Camera _camera;


    private void Awake()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _camera = GameObject.Find("Main Camera").GetComponent<Camera>();
    }
    
    private void FixedUpdate()
    {
        _rigidbody2D.MovePosition(_rigidbody2D.position + movement * speed * Time.deltaTime); // gerak
        Vector2 lookDir = mousePos - _rigidbody2D.position;
        float angleLook = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg;
        _rigidbody2D.rotation = angleLook;
    }
    
    // Update is called once per frame
    void Update()
    {
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");
        mousePos = _camera.ScreenToWorldPoint(Input.mousePosition);
    }
}
